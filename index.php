<?php

require_once 'Image.php';
define('IMAGEPATH', 'images/');

$sizes = getSizes();
$image = resize(getRandomImage(), $sizes['width'], $sizes['height']);

header('Content-Type: image/jpeg');
header("HTTP/1.1 200 OK");
imagejpeg(readfile($image, 'r'));



function getRandomImage() 
{
    $images = [];
    foreach(glob(IMAGEPATH.'*') as $filename){
        $images[] =  '/' . basename($filename);
    }
    return $images[rand(0, count($images)-1)];
}

function getSizes()
{
    preg_match("/([0-9]+)x([0-9]+)/", $_SERVER['REQUEST_URI'], $arr);

    return [
        'width' => isset($arr[1]) ? $arr[1] : 200,
        'height' => isset($arr[2]) ? $arr[2] : 200,
    ];
}

function resize($filename, $width, $height) {
    $filename = trim($filename, '/');
    if (!is_file(IMAGEPATH . $filename)) {
        return;
    }
    $extension = pathinfo($filename, PATHINFO_EXTENSION);
    $old_image = $filename;
    $new_image = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
    if (!is_file(IMAGEPATH . $new_image) || (filectime(IMAGEPATH . $old_image) > filectime(IMAGEPATH . $new_image))) {
        $path = '';
        $directories = explode('/', dirname(str_replace('../', '', $new_image)));
        foreach ($directories as $directory) {
            $path = $path . '/' . $directory;
            if (!is_dir(IMAGEPATH . $path)) {
                @mkdir(IMAGEPATH . $path, 0777);
            }
        }
        list($width_orig, $height_orig) = getimagesize(IMAGEPATH . $old_image);
        if ($width_orig != $width || $height_orig != $height) {
            $image = new Image(IMAGEPATH . $old_image);
            $image->resize($width, $height);
            $image->save(IMAGEPATH . $new_image);
        } else {
            copy(IMAGEPATH . $old_image, IMAGEPATH . $new_image);
        }
    }

    return IMAGEPATH . $new_image;
}